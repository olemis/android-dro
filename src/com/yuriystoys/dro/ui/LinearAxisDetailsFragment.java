package com.yuriystoys.dro.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.callbacks.IConnectionStateChangedCallback;
import com.yuriystoys.dro.callbacks.IPositionChangedCallback;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.tools.Tool;
import com.yuriystoys.dro.ui.util.NearZeroWarning;

@SuppressLint("ValidFragment")
public class LinearAxisDetailsFragment extends DialogFragment implements IPositionChangedCallback,
		IConnectionStateChangedCallback {

	AxisSettings axis;
	Dro dro;
	private TextView _position;
	private TextView _feedRate;
	private TextView _chipload;

	long lastTime = 0;
	int lastPosition = 0;
	double feedRate = 0D;
	double rpm = 0D;
	int toothCount = 1;
	Tool selectedTool = null;
	boolean soundOn = false;
	
	NearZeroWarning warning;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		dro = DroApplication.getCurrentInstance().getDro();
		warning = new NearZeroWarning(axis);
		rpm = dro.getAxis(Axis.T).getReadout();
		
		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_linear_axis_details, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(axis.getLabel() + " Axis Details");
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false).setNegativeButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {

								dismiss();
							}
						});
			}
		});

		ImageButton halfButton = (ImageButton) view.findViewById(R.id.halfButton);

		halfButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int halfPosition = axis.getPosition() / 2;

				axis.setOffset(axis.getIncrementalOffset() - halfPosition);

				axis.setIncrementalMode();
			}
		});

		ImageButton invertButton = (ImageButton) view.findViewById(R.id.invertButton);

		invertButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int readout = axis.getPosition();

				axis.setOffset(axis.getIncrementalOffset() - readout * 2);

				axis.setIncrementalMode();
			}
		});

		ImageButton zeroButton = (ImageButton) view.findViewById(R.id.zeroSetButton);

		zeroButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				axis.setIncrementalOrigin();
				axis.setIncrementalMode();
			}
		});
		
		zeroButton.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {

				DialogFragment dialog = new DialogFragment() {

					@Override
					public Dialog onCreateDialog(Bundle savedInstanceState) {
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

						builder.setTitle(R.string.origin_dialog_title).setMessage(R.string.origin_dialog_message_single)
								.setPositiveButton(R.string.yes_button, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										axis.setWorkspaceOrigin();
									}
								}).setNegativeButton(R.string.no_button, null);
						// Create the AlertDialog object and return it
						return builder.create();
					}

				};
				dialog.show(getActivity().getFragmentManager(), "Dialog");

				return true;
			}
		});
		
		((Button)view.findViewById(R.id.position)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				LinearAxisToolsDialog.Show(getActivity(), axis.getAxis());

			}
		});
		
		final ImageButton toggleSound = (ImageButton) view.findViewById(R.id.toggleSoundButton);

		toggleSound.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				soundOn = !soundOn;
				
				//TODO: Set button style
				
				if(soundOn)
				{
					toggleSound.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sound_on));
					warning.start();
				}	
				else
				{
					toggleSound.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sound_off));
					warning.stop();
				}
			}
		});

		Repository repo = Repository.open(getActivity());
		selectedTool = repo.getTool(dro.getSelectedToolId());

		if (selectedTool != null) {
			toothCount = selectedTool.getToothCount() != 0 ? selectedTool.getToothCount() : 1;
			if (dro.axisEnabled(Axis.T)) {
				dro.getAxis(Axis.T).registerCallback((IPositionChangedCallback) this);
			}
		}

		setUpUi(view);

		axis.registerCallback((IPositionChangedCallback) this);
		onPositionChanged(axis);

		return dialog;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onDismiss(dialog);
		
		warning.stop();

		axis.removeCallback((IPositionChangedCallback) this);

		if (dro.axisEnabled(Axis.T) && selectedTool != null) {
			dro.getAxis(Axis.T).removeCallback((IPositionChangedCallback) this);
		}
	}

	private void setUpUi(View view) {
		_position = (TextView) view.findViewById(R.id.position);
		_feedRate = (TextView) view.findViewById(R.id.feedRate);
		_chipload = (TextView) view.findViewById(R.id.chiopLoad);

		String posUnits = view.getContext().getResources()
				.getString(dro.isInMetricMode() ? R.string.mm : R.string.inch);
		String feedUnits = view.getContext().getResources()
				.getString(dro.isInMetricMode() ? R.string.mmpm : R.string.ipm);
		String chipLoadUnits = view.getContext().getResources()
				.getString(dro.isInMetricMode() ? R.string.mm_per_tooth : R.string.in_per_tooth);

		((TextView) view.findViewById(R.id.positionUnits)).setText(posUnits);
		((TextView) view.findViewById(R.id.feedUnits)).setText(feedUnits);
		((TextView) view.findViewById(R.id.chiploadUnits)).setText(chipLoadUnits);

		if (dro.axisEnabled(Axis.T) && selectedTool != null) {
			_chipload.setEnabled(true);
			((TextView) view.findViewById(R.id.chiploadUnits)).setEnabled(true);

		} else {
			_chipload.setEnabled(false);
			((TextView) view.findViewById(R.id.chiploadUnits)).setEnabled(false);
			Toast.makeText(getActivity(),
					"Chip load fuinction not available. Enable tachometer and select a tool to activate it.",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void Show(Activity activity, int axis) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		LinearAxisDetailsFragment fragment = new LinearAxisDetailsFragment();
		fragment.axis = DroApplication.getCurrentInstance().getDro().getAxis(axis);

		fragment.show(ft, "dialog");
	}

	@SuppressWarnings("unused")
	private boolean isEmpty(EditText etText) {
		return etText.getText().toString().trim().length() == 0;
	}

	@Override
	public void onConnectionStateChanged(int status) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPositionChanged(AxisSettings sender) {

		if (sender.getAxis() != Axis.T) {
			long currentTime = System.currentTimeMillis();

			_position.setText(dro.getCurrentFormat().format(sender.getReadout()));

			long deltaTime = currentTime - lastTime;

			if (deltaTime > 500) {
				// calculate feed rate every 0.5 seconds
				int position = sender.getRawPosition();

				double deltaPosition = sender.convertToDimension(position - lastPosition);

				feedRate = deltaPosition / deltaTime * 60000;

				_feedRate.setText(dro.getCurrentFormat().format(feedRate));

				lastPosition = position;
				lastTime = currentTime;
				displayChipload();
			}
		} else {
			rpm = sender.getReadout();
			displayChipload();
		}
	}

	private void displayChipload() {
		_chipload.setText(dro.getCurrentFormat().format(rpm != 0 && toothCount != 0 ? Math.abs(feedRate / rpm / toothCount) : 0));
	}
}
