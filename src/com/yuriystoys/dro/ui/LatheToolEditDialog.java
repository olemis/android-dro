package com.yuriystoys.dro.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.tools.Tool;
import com.yuriystoys.dro.ui.util.ToolsAdapter;

@SuppressLint("ValidFragment")
public class LatheToolEditDialog extends DialogFragment {

	public static final String TOOL_ID_KEY = "tool_id";
	private int toolId = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_lathe_tool_edit, null);

		setUpUi(view);
		if(toolId>=0)
			showTool(toolId);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.tool_ofset_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false).setPositiveButton(R.string.lathe_tool_edit_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// do nothing. The real handler is below
			}
		}).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (saveTool())
									dismiss();
							}
						});
			}
		});

		return dialog;
	}

	private EditText _toolNameEdit;
	private EditText _toolDescriptionEdit;
	private EditText _toolXOffsetEdit;
	private EditText _toolZOffsetEdit;

	private void setUpUi(View view) {
		Dro dro = DroApplication.getCurrentInstance().getDro();

		_toolNameEdit = (EditText) view.findViewById(R.id.toolNameEdit);
		_toolDescriptionEdit = (EditText) view.findViewById(R.id.toolDescriptionEdit);
		_toolXOffsetEdit = (EditText) view.findViewById(R.id.toolXOffsetEdit);
		_toolZOffsetEdit = (EditText) view.findViewById(R.id.toolZOffsetEdit);

		_toolXOffsetEdit.setText(dro.getCurrentFormat().format(0));
		_toolZOffsetEdit.setText(dro.getCurrentFormat().format(0));

		String units = view.getContext().getResources().getString(dro.isInMetricMode() ? R.string.mm : R.string.inch);

		((TextView) view.findViewById(R.id.xOffsetUnits)).setText(units);
		((TextView) view.findViewById(R.id.zOffsetUnits)).setText(units);

	}
	
	private boolean showTool(int toolId)
	{
		Dro dro = DroApplication.getCurrentInstance().getDro();
		Repository repo = Repository.open(getActivity());
		
		Tool tool = repo.getTool(toolId);
		if(tool==null)
			return false;
		
		_toolNameEdit.setText(tool.getName());
		_toolDescriptionEdit.setText(tool.getDescription());
	
		double offset = dro.isInMetricMode() ? tool.getOffsetX() * Dro.MM_PER_INCH : tool.getOffsetX();
		
		_toolXOffsetEdit.setText(dro.getCurrentFormat().format(offset));
		
		offset = dro.isInMetricMode() ? tool.getOffsetZ() * Dro.MM_PER_INCH : tool.getOffsetZ();
		
		_toolZOffsetEdit.setText(dro.getCurrentFormat().format(offset));
		
		return true;
	}

	private boolean saveTool() {

		if (isEmpty(_toolNameEdit)) {
			Toast.makeText(getActivity(), "Tool name is required.", Toast.LENGTH_LONG).show();
			_toolNameEdit.requestFocus();
			return false;
		}
		
		Dro dro = DroApplication.getCurrentInstance().getDro();

		String name = _toolNameEdit.getText().toString();
		String description = _toolDescriptionEdit.getText().toString();
		double xOffset = 0;
		double zOffset = 0;
		
		if(!isEmpty(_toolXOffsetEdit))
		{
			String tempX = _toolXOffsetEdit.getText().toString();
			
			try {
				xOffset = Double.parseDouble(tempX);
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), "Please enter a valid tool X offset.", Toast.LENGTH_LONG).show();
				_toolXOffsetEdit.requestFocus();
				return false;
			}

			if (dro.isInMetricMode()) {
				// convert to inches
				xOffset = xOffset / Dro.MM_PER_INCH;
			}
		}
		
		if(!isEmpty(_toolZOffsetEdit))
		{
			String tempZ = _toolZOffsetEdit.getText().toString();
			
			try {
				zOffset = Double.parseDouble(tempZ);
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), "Please enter a valid tool Z offset.", Toast.LENGTH_LONG).show();
				_toolZOffsetEdit.requestFocus();
				return false;
			}

			if (dro.isInMetricMode()) {
				// convert to inches
				zOffset = zOffset / Dro.MM_PER_INCH;
			}
		}
		
		if(xOffset == 0 && zOffset == 0)
		{
			Toast.makeText(getActivity(), "Either X or Z offset is required.", Toast.LENGTH_LONG).show();
			_toolZOffsetEdit.requestFocus();
			return false;
		}
		
		
		// save the tool

		Tool tool = new Tool();

		tool.setId(toolId);
		tool.setName(name);
		tool.setDescription(description);
		tool.setToothCount(1);
		tool.setOffsetX(xOffset);
		tool.setOffsetZ(zOffset);
		
		tool.setType(MachineTypes.LATHE);

		Repository repo = Repository.open(getActivity());

		if (toolId <= 0 && repo.insert(tool) == 0) {
			Toast.makeText(getActivity(), "Failed to create a new tool record.", Toast.LENGTH_LONG).show();
			return false;
		} else if (toolId > 0 && !repo.update(tool)) {
			Toast.makeText(getActivity(), "Failed to update the tool record.", Toast.LENGTH_LONG).show();
			return false;
		}

		ToolsAdapter.raiseOnToolListChanged();
		return true;
	}

	protected LatheToolEditDialog(int toolId) {
		this.toolId = toolId;
	}

	public static void Show(Activity activity, int toolId) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new LatheToolEditDialog(toolId);
		newFragment.show(ft, "dialog");
	}

	public static void Show(Activity activity) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new LatheToolEditDialog(-1);
		newFragment.show(ft, "dialog");
	}

	private boolean isEmpty(EditText etText) {
		return etText.getText().toString().trim().length() == 0;
	}
}
