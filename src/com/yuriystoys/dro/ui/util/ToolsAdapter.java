package com.yuriystoys.dro.ui.util;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.callbacks.IToolListChangedCallback;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.Dro.ReadoutFormat;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.tools.Tool;

public class ToolsAdapter extends ArrayAdapter<ToolsAdapter.ToolViewModel> {
	int layoutResourceId;

	final Dro readout;

	public ToolsAdapter(Context context, Dro dro, int layoutResourceId, List<ToolViewModel> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.readout = dro;
		
	}

	public void refresh() {
		super.clear();

		Dro dro = DroApplication.getCurrentInstance().getDro();

		List<Tool> tools = Repository.open(context).getTools(dro.getMachineType());

		for (Tool tool : tools)
			super.add(new ToolsAdapter.ToolViewModel(tool, tool.getId().equals(dro.getSelectedToolId())));

		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		return getDropDownView(position, convertView, parent);
	}

	Context context;
	final static String coordFormatMm = "";
	final static String coordFormatInch = "";

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		ToolViewModel tool = super.getItem(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
		}

		TextView title = ((TextView) row.findViewById(R.id.title_text_view));
		TextView descr = ((TextView) row.findViewById(R.id.description_text_view));
		TextView coord = ((TextView) row.findViewById(R.id.coordinate_text_view));

		if (tool.selected) {
			title.setTypeface(null, Typeface.BOLD);
			title.setTextColor(parent.getContext().getResources().getColor(android.R.color.holo_green_light));
			// descr.setTextColor(parent.getContext().getResources().getColor(android.R.color.holo_green_light));
			coord.setTextColor(parent.getContext().getResources().getColor(android.R.color.holo_green_light));
		} else {
			title.setTypeface(null, Typeface.NORMAL);
			title.setTextColor(parent.getContext().getResources().getColor(android.R.color.primary_text_dark));
			// descr.setTextColor(parent.getContext().getResources().getColor(android.R.color.primary_text_dark));
			coord.setTextColor(parent.getContext().getResources().getColor(android.R.color.primary_text_dark));
		}

		title.setText(tool.name);

		/*
		 * if (tool.description != null) { descr.setText(tool.description);
		 * descr.setVisibility(View.VISIBLE); } else {
		 * descr.setVisibility(View.GONE); }
		 */

		descr.setVisibility(View.GONE);

		coord.setText(tool.toString(readout));

		return row;
	}

	// private method of your class
	public int getIndex(int toolId) {
		for (int i = 0; i < this.getCount(); i++) {
			if (this.getItem(i).id == toolId) {
				return i;
			}
		}
		return -1;
	}

	private static List<IToolListChangedCallback> callbacks = new ArrayList<IToolListChangedCallback>();

	public static void registerCallback(IToolListChangedCallback callback) {
		if (!callbacks.contains(callback)) {
			callbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	public static void removeCallback(IToolListChangedCallback callback) {
		if (callbacks.contains(callback))
			callbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public static void raiseOnToolListChanged() {
		for (IToolListChangedCallback callback : callbacks) {
			callback.onToolListChangedChanged();
		}
	}

	public class ToolViewModel {

		public Integer id;
		public MachineTypes type;
		public String name;
		public String description;
		public double fluteDiameter;
		public double shankDiameter;
		public double fluteLength;
		public int toothCount;
		public double offsetX;
		public double offsetY;
		public double offsetZ;
		public boolean selected;

		public ToolViewModel(Tool tool, boolean selected) {

			id = tool.getId();
			type = tool.getType();
			name = tool.getName();
			description = tool.getDescription();
			fluteDiameter = tool.getFluteDiameter();
			shankDiameter = tool.getShankDiameter();
			fluteLength = tool.getFluteLength();
			toothCount = tool.getToothCount();
			offsetX = tool.getOffsetX();
			offsetY = tool.getOffsetY();
			offsetZ = tool.getOffsetZ();
			this.selected = selected;
		}

		@SuppressLint("DefaultLocale")
		public String toString(Dro dro) {

			String units = context.getResources().getString(dro.isInMetricMode() ? R.string.mm : R.string.inch);
			ReadoutFormat format = dro.getCurrentFormat();

			switch (dro.getMachineType()) {
			case VERTICAL_MILL: {
				double diameter = dro.isInMetricMode() ? fluteDiameter * Dro.MM_PER_INCH : fluteDiameter;
				double offset = dro.isInMetricMode() ? offsetZ * Dro.MM_PER_INCH : offsetZ;

				return Integer.toString(toothCount) + " fl. - " + format.format(diameter) + " " + units
						+ (offset != 0 ? (", Z: " + format.format(offset) + " " + units) : "");
			}
			case LATHE: {
				double xOffset = dro.isInMetricMode() ? offsetX * Dro.MM_PER_INCH : offsetX;
				double zOffset = dro.isInMetricMode() ? offsetZ * Dro.MM_PER_INCH : offsetZ;

				return "Offset X:" + format.format(xOffset) + " " + units + ", Z: " + format.format(zOffset) + " "
						+ units;
			}
			default:
				return this.description;

			}
		}
	}
}
